#ifndef LISP_H_INCLUDED
#define LISP_H_INCLUDED

namespace lisp {

	/* We begin by defining four types of objects we will be
	 * using. CONS is what we use to hold lists, ATOMs are letters or
	 * digits anything that is not used by LISP, a FUNC holds a
	 * reference to a C function and a LAMBDA holds a lambda
	 * expression. */

	struct object;

	// TO DO : ADD SYMBOL, FIXNUM, FLOATNUM
	enum kind {
		e_ATOM = 0,
		e_CONS,
		e_FUNC,
		e_LAMBDA,
		e_SYMBOL
	};

	struct atom {
		std::string name;

		atom(const std::string& n) : name(n) {
		}
	};

	struct cons {
		boost::shared_ptr<object> car;
		boost::shared_ptr<object> cdr;
		
		cons(boost::shared_ptr<object> first, boost::shared_ptr<object> second) : car(first), cdr(second) {

		}
	};

	typedef boost::function< boost::shared_ptr<object> (boost::shared_ptr<object>,boost::shared_ptr<object>) > lisp_func;

	struct func {
		lisp_func fn;

		func(const lisp_func& f) : fn(f) {

		}
	};

	struct lambda {
		boost::shared_ptr<object> args;
		boost::shared_ptr<object> sexp;

		lambda(boost::shared_ptr<object> a, boost::shared_ptr<object> s) : args(a), sexp(s) {

		}
	};

	struct symbol {
		boost::shared_ptr<object> value;
		
		symbol(boost::shared_ptr<object> v) : value(v) {
		}
	};
		
	typedef boost::variant< atom, cons, func, lambda, symbol > base_object;

	struct object : public base_object {

		object(const base_object&b) : base_object(b) {

		}
		
		object(const std::string&s) : base_object(atom(s)) {

		}

	};

	extern boost::shared_ptr<object> tee;
	extern boost::shared_ptr<object> nil;
	
};

#endif
