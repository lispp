#ifndef ENV_H
#define ENV_H


/** This is where symbols live - essentially a hash table of name:value mapping a symbol name to a variable value */
namespace lisp {


	/** Equality predicate for an atom */
	struct iequal_to
				: std::binary_function<boost::shared_ptr<const atom>, boost::shared_ptr<const atom>, bool> {
		bool operator()(boost::shared_ptr<const atom> ax,
						boost::shared_ptr<const atom> ay) const {
			std::string const& x(ax->name);
			std::string const& y(ay->name);
			return boost::algorithm::iequals(x, y, std::locale());
		}
	};

	/** Hash function for an atom **/
	struct ihash
				: std::unary_function<std::string, std::size_t> {
		std::size_t operator()(boost::shared_ptr<const atom> ax) const {
			std::string const& x(ax->name);
			std::size_t seed = 0;
			std::locale locale;
			for (std::string::const_iterator it = x.begin();
					it != x.end(); ++it) {
				boost::hash_combine(seed, std::toupper(*it, locale));
			}
			return seed;
		}
	};


	/** The environment proper */
	class environment 	{
	private:
		typedef boost::unordered_map<boost::shared_ptr<const atom>, boost::shared_ptr<symbol>, ihash, iequal_to> environment_type;

		environment_type env_map;
		boost::shared_ptr<environment> parent;

	public:

		/** Default constructor for root environment */
		environment() : parent() {
		};

		/**
		 * Main construtor for further outer environments
		 * @param inner inner environment that this encloses
		 */
		environment(boost::shared_ptr<environment> inner) : parent(inner) {
		}

		/**
		 * Variable assignment
		 * @param a Atom representing name of symbol to assign to
		 * @param s Symbol we assign to the atom in this environment
		 */
		void set(boost::shared_ptr<const atom> a, boost::shared_ptr<symbol> s) {
			env_map[a] = s;
		};

		/**
		 * Variable access
		 * @param a Atom represnting name of symbol to access
		 * @return Symbol in environment or nil if none found
		 */
		boost::shared_ptr<symbol> get(boost::shared_ptr<const atom> a) const {
			environment_type::const_iterator lookup(env_map.find(a));
			if (lookup == env_map.end()) {
				if (parent != NULL) {
					return parent->get(a);
				} else {
					return boost::shared_ptr<symbol>(new symbol(nil));
				}
			} else {
				return lookup->second;
			}
		}

	};
}
#endif
