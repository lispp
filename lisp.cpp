
#include <cctype>
#include <iostream>
#include <fstream>
#include <boost/variant.hpp>
#include <boost/function.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/unordered_map.hpp>
#include <boost/algorithm/string.hpp>

#include "lisp.h"
#include "env.h"

std::string name(boost::shared_ptr<lisp::object> token) {
	return boost::get<lisp::atom>(*token).name;
};

boost::shared_ptr<lisp::object>& car(boost::shared_ptr<lisp::object> x)  {

	return boost::get<lisp::cons>(*(boost::static_pointer_cast<lisp::base_object, lisp::object>(x))).car;
}

boost::shared_ptr<lisp::object>& cdr(boost::shared_ptr<lisp::object> x)  {
	return boost::get<lisp::cons>(*(boost::static_pointer_cast<lisp::base_object, lisp::object>(x))).cdr;
}

boost::shared_ptr<lisp::object> make_cons(boost::shared_ptr<lisp::object> car,
        boost::shared_ptr<lisp::object> cdr) {
	return boost::shared_ptr<lisp::object>(new lisp::object(lisp::cons(car, cdr)));
}

boost::shared_ptr<lisp::object> make_lambda(boost::shared_ptr<lisp::object> args,
        boost::shared_ptr<lisp::object> sexp) {
	return boost::shared_ptr<lisp::object>(new lisp::object(lisp::lambda(args, sexp)));
}

void append(boost::shared_ptr<lisp::object> list, boost::shared_ptr<lisp::object> obj) {
	boost::shared_ptr<lisp::object> ptr;
	ptr = list;
	while (cdr(ptr) != NULL) {
		ptr = cdr(ptr);
	}
	cdr(ptr) = boost::shared_ptr<lisp::object>(make_cons(obj, boost::shared_ptr<lisp::object>()));
}

boost::shared_ptr<lisp::object> fn_car(boost::shared_ptr<lisp::object> args,
                                       boost::shared_ptr<lisp::object> env) {
	return car(car(args));
}

boost::shared_ptr<lisp::object> fn_cdr(boost::shared_ptr<lisp::object> args,
                                       boost::shared_ptr<lisp::object> env) {
	return cdr(car(args));
}

boost::shared_ptr<lisp::object> fn_quote(boost::shared_ptr<lisp::object> args,
        boost::shared_ptr<lisp::object> env) {
	return car(args);
}

boost::shared_ptr<lisp::object> fn_cons(boost::shared_ptr<lisp::object> args,
                                        boost::shared_ptr<lisp::object> env) {

	boost::shared_ptr<lisp::object> list = make_cons(car(args), boost::shared_ptr<lisp::object>());
	args = car(cdr(args));

	while ((args != NULL) && (args->which() == lisp::e_CONS)) {
		append(list, car(args));
		args = cdr(args);
	}
	return list;
}

boost::shared_ptr<lisp::object> tee;
boost::shared_ptr<lisp::object> nil;

boost::shared_ptr<lisp::object>  fn_equal(boost::shared_ptr<lisp::object> args,
        boost::shared_ptr<lisp::object> env) {
	boost::shared_ptr<lisp::object> first = car(args);
	boost::shared_ptr<lisp::object> second = car(cdr(args));
	if (name(first) == name(second))
		return tee;
	else
		return nil;
}

boost::shared_ptr<lisp::object> fn_atom(boost::shared_ptr<lisp::object> args, boost::shared_ptr<lisp::object> env) {
	if(car(args)->which() == lisp::e_ATOM)
		return tee;
	else
		return nil;
}

boost::shared_ptr<lisp::object> eval(boost::shared_ptr<lisp::object> args,
                                     boost::shared_ptr<lisp::object> env);

boost::shared_ptr<lisp::object> fn_cond(boost::shared_ptr<lisp::object> args,
                                        boost::shared_ptr<lisp::object> env) {
	while ((args != NULL) && (args->which() == lisp::e_CONS)) {
		boost::shared_ptr<lisp::object> list = car(args);
		boost::shared_ptr<lisp::object> pred = eval(car(list), env);
		boost::shared_ptr<lisp::object> ret = car(cdr(list));

		if(pred != nil)
			return eval(ret,env);

		args = cdr(args);
	}

	return nil;
}

boost::shared_ptr<lisp::object> interleave (boost::shared_ptr<lisp::object>	c1,
        boost::shared_ptr<lisp::object> c2) {

	boost::shared_ptr<lisp::object> nul;
	boost::shared_ptr<lisp::object> list = make_cons(make_cons(car(c1),make_cons(car(c2),nul)),nul);
	c1 = cdr(c1);
	c2 = cdr(c2);

	while ((c1 != NULL) && (c1->which() == lisp::e_CONS)) {
		append(list,make_cons(car(c1),make_cons(car(c2), nul)));
		c1 = cdr(c1);
		c2 = cdr(c2);
	}

	return list;
}

boost::shared_ptr<lisp::object> replace_atom(boost::shared_ptr<lisp::object> sexp,
        boost::shared_ptr<lisp::object> with)  {

	boost::shared_ptr<lisp::object> nul;

	if(sexp->which() == lisp::e_CONS) {

		boost::shared_ptr<lisp::object> list = make_cons(replace_atom(car(sexp), with), nul);
		sexp = cdr(sexp);

		while ((sexp != NULL) && (sexp->which() ==lisp:: e_CONS)) {
			append(list,replace_atom(car(sexp), with));
			sexp = cdr(sexp);
		}

		return list;

	} else {
		boost::shared_ptr<lisp::object> tmp = with;

		while ((tmp != NULL) && (tmp->which() == lisp::e_CONS)) {
			boost::shared_ptr<lisp::object> item = car(tmp);
			boost::shared_ptr<lisp::object> atom = car(item);
			boost::shared_ptr<lisp::object> replacement = car(cdr(item));

			if (name(atom) == name(sexp))
				return replacement;

			tmp = cdr(tmp);
		}

		return sexp;
	}
}

boost::shared_ptr<lisp::object> fn_lambda (boost::shared_ptr<lisp::object>args,
        boost::shared_ptr<lisp::object>env) {
	boost::shared_ptr<lisp::object> lambda = car(args);
	args = cdr(args);

	lisp::lambda& lambda_object(boost::get<lisp::lambda>(*(boost::static_pointer_cast<lisp::base_object, lisp::object>(lambda))));

	boost::shared_ptr<lisp::object> list = interleave(lambda_object.args, args);
	boost::shared_ptr<lisp::object> sexp = replace_atom(lambda_object.sexp,list);
	return eval(sexp,env);
}

boost::shared_ptr<lisp::object> fn_label (boost::shared_ptr<lisp::object> args,
        boost::shared_ptr<lisp::object> env) {
	boost::shared_ptr<lisp::object> nul;

	std::string n(name(car(args)));
	boost::shared_ptr<lisp::object> a(new lisp::object(n));
	append(env, make_cons(a,
	                      make_cons(car(cdr(args)),
	                                nul)));
	return tee;
}

boost::shared_ptr<lisp::object> lookup(const std::string& n,
                                       boost::shared_ptr<lisp::object> env) {
	boost::shared_ptr<lisp::object> nul;
	boost::shared_ptr<lisp::object>tmp = env;

	while ((tmp != nul) && (tmp->which()) == lisp::e_CONS) {
		boost::shared_ptr<lisp::object> item = car(tmp);
		boost::shared_ptr<lisp::object> nm   = car(item);
		boost::shared_ptr<lisp::object> val  = car(cdr(item));

		if (name(nm) == n)
			return val;
		tmp = cdr(tmp);
	}
	return nul;
}

// I.O. I.O. It's off to port we go..

void print(std::ostream& out, boost::shared_ptr<lisp::object> sexp) {

	if(sexp == NULL)
		return;
	if(sexp->which() == lisp::e_CONS) {
		out.put('(');
		print(out, car(sexp));
		sexp = cdr(sexp);
		while ((sexp != NULL) && (sexp->which() == lisp::e_CONS)) {
			out.put(' ');
			print(out, car(sexp));
			sexp = cdr(sexp);
		}
		out.put(')');
	} else if (sexp->which() == lisp::e_ATOM) {
		out << name(sexp);
	} else if (sexp->which() == lisp::e_LAMBDA) {
		out.put('#');
		out << (boost::get<lisp::lambda>(*sexp)).args;
		out << (boost::get<lisp::lambda>(*sexp)).sexp;
	} else
		printf ("Error.");
}


boost::shared_ptr<lisp::object> next_token(std::istream& in) {
	char c;

	// skip whitespace
	do {
		c = in.get();
	} while (isspace(c));
	// process first char
	std::string tok;
	// if it is a paren, look no further
	if ((c == ')') || ( c== '(')) {
		tok.push_back(c);
	} else {
		// otherwise collect an atom
		do {
			tok.push_back(c);
			c = in.get();
		} while ((!isspace(c)) && (c != ')'));
		in.unget();
	}
	return boost::shared_ptr<lisp::object>(new lisp::object(tok));
}

boost::shared_ptr<lisp::object> read_tail(std::istream& in) {
	boost::shared_ptr<lisp::object> token(next_token(in));
	if (name(token) == ")")
		return boost::shared_ptr<lisp::object>();
	else if (name(token) == "(") {
		boost::shared_ptr<lisp::object> car = read_tail(in);
		boost::shared_ptr<lisp::object> cdr = read_tail(in);
		return make_cons(car, cdr);
	} else {
		boost::shared_ptr<lisp::object> car = token;
		boost::shared_ptr<lisp::object> cdr = read_tail(in);
		return make_cons(car, cdr);
	}
};

/* read gets the next token from the file, if it is a left parentheses
 * it calls read_tail to parse the rest of the list, otherwise returns
 * the token read. A list (LIST e1 ... en) is defined for each n to be
 * (CONS e1 (CONS ... (CONS en NIL))) so read_tail will keep calling
 * itself concatenating cons cells until it hits a right
 * parentheses. */

boost::shared_ptr<lisp::object> read(std::istream& in) {
	boost::shared_ptr<lisp::object> token = next_token(in);
	if (name(token) == "(")
		return read_tail(in);
	return token;
}



boost::shared_ptr<lisp::object> init_env() {

	boost::shared_ptr<lisp::object> nul;

	boost::shared_ptr<lisp::object> a_quote(new lisp::object(std::string("QUOTE")));
	boost::shared_ptr<lisp::object> f_quote(new lisp::object(lisp::func(fn_quote)));

	boost::shared_ptr<lisp::object> a_car(new lisp::object(std::string("CAR")));
	boost::shared_ptr<lisp::object> f_car(new lisp::object(lisp::func(fn_car)));

	boost::shared_ptr<lisp::object> a_cdr(new lisp::object(std::string("CDR")));
	boost::shared_ptr<lisp::object> f_cdr(new lisp::object(lisp::func(fn_cdr)));

	boost::shared_ptr<lisp::object> a_cons(new lisp::object(std::string("CONS")));
	boost::shared_ptr<lisp::object> f_cons(new lisp::object(lisp::func(fn_cons)));

	boost::shared_ptr<lisp::object> a_equal(new lisp::object(std::string("EQUAL")));
	boost::shared_ptr<lisp::object> f_equal(new lisp::object(lisp::func(fn_equal)));

	boost::shared_ptr<lisp::object> a_atom(new lisp::object(std::string("ATOM")));
	boost::shared_ptr<lisp::object> f_atom(new lisp::object(lisp::func(fn_atom)));

	boost::shared_ptr<lisp::object> a_cond(new lisp::object(std::string("COND")));
	boost::shared_ptr<lisp::object> f_cond(new lisp::object(lisp::func(fn_cond)));

	boost::shared_ptr<lisp::object> a_lambda(new lisp::object(std::string("LAMBDA")));
	boost::shared_ptr<lisp::object> f_lambda(new lisp::object(lisp::func(fn_lambda)));

	boost::shared_ptr<lisp::object> a_label(new lisp::object(std::string("LABEL")));
	boost::shared_ptr<lisp::object> f_label(new lisp::object(lisp::func(fn_label)));


	boost::shared_ptr<lisp::object> env = make_cons(make_cons(a_quote,make_cons(f_quote,nul)),nul);


	append(env,make_cons(a_car,	  make_cons(f_car,nul)));
	append(env,make_cons(a_cdr,	  make_cons(f_cdr,nul)));
	append(env,make_cons(a_cons,  make_cons(f_cons,nul)));
	append(env,make_cons(a_equal, make_cons(f_equal,nul)));
	append(env,make_cons(a_atom,  make_cons(f_atom,nul)));
	append(env,make_cons(a_cond,  make_cons(f_cond,nul)));
	append(env,make_cons(a_lambda, make_cons(f_lambda,nul)));
	append(env,make_cons(a_label,  make_cons(f_label,nul)));

	boost::shared_ptr<lisp::object> a_tee(new lisp::object(std::string("#T")));
	tee = a_tee;
	nil = make_cons(nul,nul);

	return env;
}


boost::shared_ptr<lisp::object> eval_fn (boost::shared_ptr<lisp::object> sexp, boost::shared_ptr<lisp::object> env) {
	boost::shared_ptr<lisp::object> symbol = car(sexp);
	boost::shared_ptr<lisp::object> args = cdr(sexp);

	if (symbol->which() == lisp::e_LAMBDA)
		return fn_lambda(sexp,env);
	else if(symbol->which() == lisp::e_FUNC)
		return ((boost::get<lisp::func>(*symbol)).fn)(args, env);
	else
		return sexp;
}

boost::shared_ptr<lisp::object> eval(boost::shared_ptr<lisp::object> sexp, boost::shared_ptr<lisp::object> env) {

	boost::shared_ptr<lisp::object> nul;

	if(sexp == NULL)
		return nil;

	if (sexp->which() == lisp::e_CONS) {
		if ((car(sexp)->which() == lisp::e_ATOM) && (name(car(sexp)) == "LAMBDA")) {
			boost::shared_ptr<lisp::object> largs = car(cdr(sexp));
			boost::shared_ptr<lisp::object> lsexp = car(cdr(cdr(sexp)));
			return make_lambda(largs,lsexp);
		} else {
			boost::shared_ptr<lisp::object> accum = make_cons(eval(car(sexp),env), nul);
			sexp = cdr(sexp);

			while ((sexp != NULL) && (sexp->which() == lisp::e_CONS)) {
				append(accum,eval(car(sexp),env));
				sexp = cdr(sexp);
			}
			return eval_fn(accum,env);
		}
	} else {
		boost::shared_ptr<lisp::object> val = lookup(name(sexp),env);
		if (val == NULL)
			return sexp;
		else
			return val;
	}
}


//
// REPL
//
int main(int argc, char *argv[]) {
	boost::shared_ptr<lisp::object> env = init_env();
	//std::istream& in(std::cin);
	std::ifstream inf;
	std::ostream& out(std::cout);
	inf.open(argv[1], std::ifstream::in);
	if (inf.good()) {
		do {
			out.put('>');
			out.put(' ');
			print(out, eval(read(inf), env));
			out << std::endl;
		} while (inf.good());
	}
	inf.close();
	return 0;
}
